import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { lang as ru } from '../translations/ru';
import { lang as en } from '../translations/en';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor (private http: HttpClient, private translate: TranslateService) {
    for (const lang of [ru, en]) {
      this.translate.setTranslation(lang.code, lang);
      if (!this.translate.getDefaultLang()) {
        this.translate.setDefaultLang(lang.code);
      }
    }
    console.log(this.translate.getDefaultLang());
  }


}
