import { Routes } from '@angular/router';
// Layouts
import { AutorizedContentComponent } from './common/autorized-content/autorized-content.component';
import { UnautorizedContentComponent } from './common/unautorized-content/unautorized-content.component';
import { AutorizedContentGuard } from './common/autorized-content/autorized-content.guard';
import { UnautorizedContentGuard } from './common/unautorized-content/unautorized-content.guard';

export const AppRoutes: Routes = [
  // {
  //   path: '',
  //   redirectTo: 'en',
  //   pathMatch: 'full'
  // },
  {
    path: '',
    children: [
      {
        path: '',
        component: AutorizedContentComponent,
        children: [
          {
            path: 'dashboard',
            loadChildren: './dashboard/dashboard.module#DashboardModule'
          }
        ],
        canActivate: [
          AutorizedContentGuard
        ]
      },
      {
        path: ':lang/login',
        component: UnautorizedContentComponent,
        children: [
          {
            path: 'dashboard',
            loadChildren: './dashboard/dashboard.module#DashboardModule'
          }
        ],
        canActivate: [
          UnautorizedContentGuard
        ]
      }
    ]
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

