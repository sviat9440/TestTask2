import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
// Layout Modules
import { AutorizedContentComponent } from './common/autorized-content/autorized-content.component';
import { UnautorizedContentComponent } from './common/unautorized-content/unautorized-content.component';
// Directives
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Sidebar_Directives } from './shared/directives/side-nav.directive';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
// Routing Module
import { AppRoutes } from './app.routing';
// App Component
import { AppComponent } from './app.component';
import { AuthService } from './shared/services/auth.service';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(AppRoutes),
    NgbModule.forRoot(),
    FormsModule,
    HttpClientModule,
    PerfectScrollbarModule,
    ToastrModule.forRoot(),
    TranslateModule.forRoot()
  ],
  declarations: [
    AppComponent,
    Sidebar_Directives,
    AutorizedContentComponent,
    UnautorizedContentComponent
  ],
  providers: [
    AuthService
  ],
  bootstrap: [
    AppComponent
  ]
})


export class AppModule {
}
