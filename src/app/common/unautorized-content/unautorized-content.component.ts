import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../shared/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-unautorized-content',
    templateUrl: './unautorized-content.component.html',
    styleUrls: [ './unautorized-content.component.scss' ]
})
export class UnautorizedContentComponent implements OnInit {

    constructor (private toastr: ToastrService,
                 private authService: AuthService,
                 private route: ActivatedRoute,
                 private translate: TranslateService) {
        this.route.params.subscribe(params => {
            if (params && params.lang) {
                this.translate.use(params.lang);
            }
        });
    }

    ngOnInit () {
    }

    async dev () {
        this.toastr.warning(await this.translate.get('dev').toPromise());
    }

}
