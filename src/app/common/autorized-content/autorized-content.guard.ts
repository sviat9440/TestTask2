import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class AutorizedContentGuard implements CanActivate {
  constructor (private router: Router, private translate: TranslateService) {}

  canActivate (next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    this.router.navigate([this.translate.getDefaultLang() || 'en', 'login']);
    return false;
  }
}
